import firebase from 'firebase'
import MobxFirebaseStore from 'mobx-firebase-store'

const config = {
    apiKey: "AIzaSyCV-iXTzF0awH0JvJZ_VxDS_AoJpnfpXLE",
    authDomain: "dinder-c3bf7.firebaseapp.com",
    databaseURL: "https://dinder-c3bf7.firebaseio.com",
    projectId: "dinder-c3bf7",
    storageBucket: "dinder-c3bf7.appspot.com",
    messagingSenderId: "550601106958"
}

// initialize Firebase SDK
export default class SettingsStore extends MobxFirebaseStore {

    constructor() {
        firebase.initializeApp(config)
        // initialize mobx
        super(firebase.database().ref())
        // create settings store data (member variables)
        this.splashTime = 1500 // milliseconds
        this.splashImg = require('../../images/splash.jpg')
        this.loginBG = require('../../images/login.jpg')
    }

    // create accessors 
    get SplashTime() {
        return this.splashTime
    }

    get SplashImg() {
        return this.splashImg
    }

    get LoginBG() {
        return this.loginBG
    }

}