// Auth store will NOT use mobx firebase store
import { observable, action } from 'mobx'
import firebase from 'firebase'

// @observable tells mobx that whenever the value of this variable changes, tell react-native to refresh any components that use it
// @action decorator tells mobx that this method will have an effect on the back end
export default class AuthStore {
    @observable authUser = null

    constructor() {
        // create firebase authentication listener (takes an observable anon function)
        // everytime logged in user changes in firebase, we'll get a new user reference
        firebase.auth().onAuthStateChanged((user) => {
            this.authUser = user;
        })
    }

    // change authUser variable    
    @action
    signIn({ email, password }) {
        if (this.authUser) {
            return Promise.resolve(this.authUser)
        }
        // if NOT logged in 
        return firebase.auth().signInWithEmailAndPassword(email, password)
    }

}
