import React, { Component } from 'react'
// drawer is side menu component
import { Drawer, View } from 'native-base'
// handles management of navigator stack, like a stack of cards
// @TODO learn to use updated navigator component
//import { Navigator } from 'react-native' // Navigator has been deprecated
//import NavigationExperimental from 'react-native-deprecated-custom-components'
import { Navigator } from 'react-native-deprecated-custom-components'

import SideMenu from './components/sideMenu'

import SettingsStore from './stores/settingsStore'
import AuthStore from './stores/authStore'

import SplashScene from './scenes/splashScene'
import LoginScene from './scenes/loginScene'
import theme from './theme/base-theme'



// create instance of settings store
const settings = new SettingsStore()
const authStore = new AuthStore()

export default class AppContainer extends Component {
    // accept argument of properties
    constructor(props) {
        super(props)
        // setup initial state        
        this.state = {
            // this state variable manages the visibility of the drawer tray
            toggled: false,
            // this will hold all our data stores
            store: {
                // add settings to store
                settings: settings,
                // make authStore available to all components and scenes
                auth: authStore
            },
            // this will be the over-arching theme file for native base
            theme: theme,
        }
    }

    // check if drawer is open or not, and call appropriate method to toggle    
    toggleDrawer() {
        this.state.toggled ? this._drawer.close() : this._drawer.open()
    }

    openDrawer() {
        this.setState({ toggled: true })
    }

    closeDrawer() {
        this.setState({ toggled: false })
    }

    renderScene(route, navigator) {
        // decide which scene to render - simplest way to handle navigation in react native
        switch (route.title) {
            case 'Splash': {
                // ... will spread properties from passProps
                return <SplashScene {...route.passProps} navigator={navigator} />
            }
            case 'Login': {
                return <LoginScene {...route.passProps} navigator={navigator} />
            }

            default: {
                return null
            }
        }
    }

    // handles how scenes are brought into view    
    configureScene(route, routeStack) {
        return Navigator.SceneConfigs.PushFromRight
        //return NavigationExperimental.Navigator.SceneConfigs.PushFromRight
    }

    // blank render method    
    render() {
        // displace value for Drawer "type" property will push content to the side instead of overlay on it
        // openDrawerOffset property defines right hand margin when drawer is opened
        return (
            <Drawer
                ref={(ref) => this._drawer = ref}
                type="displace"
                content={<SideMenu navigator={this._navigator} theme={this.state.theme} />}
                onClose={this.closeDrawer.bind(this)}
                onOpen={this.openDrawer.bind(this)}
                panOpenMask={.05}
                openDrawerOffset={0.2}>

                <Navigator
                    ref={(ref) => this._navigator = ref}
                    configureScene={this.configureScene.bind(this)}
                    renderScene={this.renderScene.bind(this)}
                    initialRoute={{
                        title: "Splash",
                        passProps: {
                            stores: this.state.store,
                            toggleDrawer: this.toggleDrawer.bind(this),
                            theme: this.state.theme
                        }
                    }} />

            </Drawer>
        )
    }

}