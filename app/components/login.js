// Handles input and login communication
import React, { Component } from 'react'
import {
    Button,
    InputGroup,
    Input,
    Icon,
    View,
    Text,
    Spinner
} from 'native-base'
import { observer } from 'mobx-react/native'

// @observer decorator tells a class to be aware of changing data from the store and re-render when needed
@observer
export default class Login extends Component {
    constructor(props) {
        super(props)
        // setup initial state
        this.state = {
            email: '',
            password: '',
            loading: null
        }
    }

    updateEmail(email) { this.setState({ email }) }

    updatePassword(password) { this.setState({ password }) }

    signIn() {
        const { auth } = this.props.stores
        const { email, password } = this.state

        // uses setState callback (trailing anon function)        
        this.setState({ loading: true }, () => {
            auth.signIn({ email, password })
                .then(() => {
                    // change scene
                    this.props.navigator.replace({
                        title: 'Match',
                        passProps: this.props
                    })
                })
        })
    }

    render() {
        const { loading } = this.state
        const { auth } = this.props.stores

        return (
            <View theme={this.props.theme}>
                <InputGroup style={{ marginBottom: 10 }} borderType='round'>
                    <Icon style={{ color: "#FFF" }} name='person-outline' />
                    <Input style={{ color: "#FFF" }}
                        placeholder='Please Enter Email'
                        placeholderTextColor='#FFF'
                        onChangeText={(email) => { this.updateEmail(email) }} />
                </InputGroup>
                <InputGroup style={{ marginBottom: 10 }} borderType='round'>
                    <Icon style={{ color: "#FFF" }} name='lock-open' />
                    <Input style={{ color: "#FFF" }}
                        placeholder='Please Enter Password'
                        placeholderTextColor='#FFF'
                        secureTextEntry={true}
                        onChangeText={(pass) => { this.updatePassword(pass) }} />
                </InputGroup>
                <Button rounded block style={{ marginBottom: 10 }} onPress={this.signIn.bind(this)}>
                    <Text>Login</Text>
                </Button>
            </View>
        )
    }

}