import React, { Component } from 'react'
import {
    Container,
    Header,
    Content,
    List,
    ListItem,
    Text,
    View,
    Button
} from 'native-base'

export default class SideMenu extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        let list = [
            {
                title: "Match",
                onPress: () => {
                    // return string that matches the title of the scene we want to render
                    this.props.navigator.replace("Match")
                }
            },
            {
                title: "History",
                // replace causes this to be a root scene - no navigation back
                onPress: () => {
                    this.props.navigator.replace("History")
                }
            }
        ]
        return (
            <Container
                theme={this.props.theme}>
                <Header />
                <View>
                    <List
                        dataArray={list}
                        renderRow={(item) =>
                            <ListItem button onPress={item.onPress.bind(this)}>
                                <Text>{item.title}</Text>
                            </ListItem>
                        } />
                </View>
            </Container>
        )
    }

}