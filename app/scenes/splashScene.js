import React, { Component } from 'react'
import { Image } from 'react-native'
import { View } from 'native-base'

export default class SplashScene extends Component {
    constructor(props) {
        super(props)
    }
    // override react lifecycle hook "componentDidMount" - fired each time a component mounts onto the DOM
    componentDidMount() {
        // using ES6 syntax to pull variable out of properties
        const { stores } = this.props
        // create a timer for our splashscreen
        setTimeout(() => {
            // using replace will disable back navigation - i.e. as root scene            
            this.props.navigator.replace({ title: "Login", passProps: this.props })
        }, stores.settings.SplashTime)
    }

    render() {
        // ES6 to pull settings from stores attribute of props
        const { settings } = this.props.stores
        // flex: 1 takes up ALL the flex space available
        return (
            <View style={{ flex: 1 }}>
                <Image style={{ flex: 1, width: null, height: null }} source={settings.SplashImg} />
            </View>
        )
    }
}